#!/bin/bash
#
#  fedora.sh
#
#  Copyright 2019-2024 asoster (https://gitlab.com/asoster/)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

set -o errexit
set -o nounset
set -o pipefail

cat >> /etc/dnf/dnf.conf << BLOQUE
defaulttypes=True
fastestmirror=True
max_parallel_downloads=10
BLOQUE

# codium repos
cat >> /etc/yum.repos.d/vscodium.repo << BLOQUE
[gitlab.com_paulcarroty_vscodium_repo]
name=download.vscodium.com
baseurl=https://download.vscodium.com/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
metadata_expire=1h
BLOQUE

# repos fusion
dnf install -y \
        https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 
dnf install -y \
        https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# repos brave-browser
dnf -y config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/ 
rpm -y --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc

dnf -y makecache

## rm /etc/yum.repos.d/file_name.repo

# Actualizamos el sistema

dnf update -y
dnf upgrade --refresh

PRENVIDIA=" kernel-devel kernel-headers gcc make dkms acpid libglvnd-glx  \
           libglvnd-opengl libglvnd-devel pkgconfig "

NVIDIA=" akmod-nvidia xorg-x11-drv-nvidia-cuda "

# dnf -y install  $NVIDIA
# tenemos que instalar todo y reiniciar, luego instalar los paquetes NVIDIA

#  radeontop clinfo opencl-utils opencl-headers conky-manager

SYSTEM=" timeshift inxi nano pwgen conky  lm_sensors iscan-firmware \
        lm_sensors-sensord hddtemp ntfs-3g \
        p7zip p7zip-plugins file-roller file-roller-nautilus \
        lvm2 cryptsetup sudo openssh sshfs nmap gparted htop numlockx  \
        gydl xz pxz \
        setroubleshoot powerline firewall-config \
        figlet policycoreutils-gui gnome-console gnome-screenshot "

VIRTUAL=" qemu-kvm virt-manager seabios edk2-ovmf \
        virt-viewer guestfs-tools libguestfs-tools virt-install genisoimage "

#  solaar piper  gnome-shell-extension-desktop-icons

GNOME=" evolution dconf-editor gnome-tweaks gnome-shell-extension-common \
        gnome-system-*  gnome-power-manager gnome-user-share  shotwell \
        gnome-photos gnome-todo hwinfo \
        gnome-sound-recorder gnome-firmware calibre \
        gnome-shell-extension-freon gnome-shell-extension-system-monitor \
        gnome-shell-extension-suspend-button gnome-shell-extension-freon  \
        neofetch variety material-design* \
        adwaita-blue-gtk-theme materia-gtk-theme \
        numix-* materia-gtk-theme deepin-gtk-theme sweet-gtk-theme "

# brasero  openshot
MEDIA=" easytag wget yt-dlp perl-Image-ExifTool transmission-gtk  \
        cdcollect  isomaster mkvtoolnix-gui multimedia-menus \
        soundconverter gaupol audacity obs-studio peek \
        simplescreenrecorder alsamixergui rocminfo pavucontrol HandBrake \
        gimp inkscape mediawriter libwebcam r5u87x-firmware \
        epson-inkjet-printer-escpr \
        dejavu-sans-mono-fonts dejavu-serif-fonts powerline-fonts \
        dejavu-sans-fonts jetbrains-mono-fonts-all cascadia-code-fonts "

BROWSERS=" chromium firefox  brave-browser  "

JUEGOS=" 0ad supertuxkart  gnome-chess gnome-sudoku gnome-mahjongg gnome-mines  "

DEVEL=" codium vim geany geany-plugins* git gitk git-svn git-gui gcc \
        python python-virtualenv python3 python3-virtualenv python-devel \
        python3-devel libpq-devel graphviz-devel libtiff-devel \
        python-pillow python3-pillow python3-pillow-devel python3-freetype
        freetype freetype-devel gettext python3-pycodestyle \
        curl python-pip ctags  ack python3-flake8 pylint python-isort \
        python3-neovim python3-tkinter  \
        gnome-builder libxml2-devel libxslt-devel libjpeg-turbo-devel "

ESPDEV=" mu esptool micropython thonny "

DRIVER=" mesa-vdpau-drivers libvdpau-va-gl libva-vdpau-driver vdpauinfo libva-utils "

# libdvdcss  ffmpeg ffmpeg-libs
#  rpmfusion-free-release-tainted 

FUSION=" vlc vlc-extras "

# Instalación de multimedia
dnf -y groupupdate multimedia 
dnf -y groupupdate sound-and-video  

dnf -y group install "Development Tools" 
dnf -y install @virtualization 

dnf -y install gstreamer1-plugins-{bad-\*,good-\*,base}
dnf -y install lame\* --exclude=lame-devel
dnf -y group upgrade --with-optional Multimedia

dnf -y group install games

dnf -y install $PRENVIDIA

dnf -y install $SYSTEM && \
    dnf -y install $GNOME  && \
    dnf -y install $MEDIA  && \
    dnf -y install $FUSION  && \
    dnf -y install $BROWSERS  && \
    dnf -y install $VIRTUAL  && \
    dnf -y install $DRIVER  && \
    dnf -y install $DEVEL  && \
    dnf -y install $JUEGOS

# enpower vim as default editor,
# dnf -y install vim-default-editor --allowerasing
