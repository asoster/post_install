#!/bin/bash
#
# fedora.work2server.sh
#
# Copyleft 2024 asoster (https://gitlab.com/asoster/)
#
# https://discussion.fedoraproject.org/t/turning-fedora-workstation-into-fedora-server/82026/3
#

set -o errexit
set -o nounset
set -o pipefail

dnf -y update

dnf -y swap fedora-release-identity-{workstation,server}
dnf -y swap fedora-release-{workstation,server}
dnf -y swap @{workstation,server}-product-environment

dnf -y group list --ids
dnf -y group remove gnome-desktop libreoffice
dnf -y remove gdm\* \*gnome\*

dnf -y group install container-management \
    network-server development-tools admin-tools system-tools \
    cloud-infrastructure cloud-management 

dnf -y install cockpit-podman

dnf -y update

