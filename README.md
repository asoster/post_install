# Script para automatizar el manejo de paquetes en tu distribución

Puñado de scripts que te pueden ayudar a instalar todo lo que necesitas para 
tener lista tu distribución GNU/Linux.

Una vez que tengas instalado tu sistema operativo GNU/Linux, y configures los
repositorios, puedes ejecutar el script bash que corresponda a tu distribución.

Debes tener permisos de root para que funcione el script.

## Instalación inicial de aplicaciones

Para Debian puedes utilizar el siguiente comando:
```
su - root -c debian.sh 

```
Para SO Fedora:
```
su - root -c fedora.sh 

```

## Script automatizado

Puedes utilizar el script `setup.sh` para realizar algunas funciones 
automáticamente. Funciona con Debian y Fedora.

Para actualizar los datos del los repositorios de la distibución:
```
su - root -c setup.sh update
```

Para instalar algun paquete, por ejemplo gimp:
```
su - root -c setup.sh install gimp
```

Para actualizar la distibución:
```
su - root -c setup.sh upgrade
```

Para actualizar la versión de la distibución:
```
su - root -c setup.sh dist-upgrade
```

