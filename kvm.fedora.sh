#!/bin/bash
#
#  kvm.fedora.sh
#
#  Copyleft 2021 asoster (https://gitlab.com/asoster/)
#


VERSION=35
FEDORAREPO="https://download.fedoraproject.org/pub/fedora/linux/releases/${VERSION}/Cloud/x86_64/images/"
QCOWIMG="Fedora-Cloud-Base-${VERSION}-1.2.x86_64.qcow2"
CHECKSUM="Fedora-Cloud-${VERSION}-1.2-x86_64-CHECKSUM"

PWROOT=$(pwgen)
RANDNAME=$(hexdump -n 5 -v -e '/1 "%02X"' /dev/urandom)
VMNAME="fedora.${RANDNAME}"
OSVARIANT="fedora-unknown"
DISKIMAGE="fedora.${RANDNAME}.qcow2"
VMHOSTNAME="vm${RANDNAME}"

if [[ "${KVMDIR}" == '' ]]; then
    KVMDIR="/var/lib/libvirt/images"
fi

# Check for commands
wget --version >> /dev/null
if [[ ! $? -eq 0 ]]; then
    echo "wget command is not installed. Exit"
    exit 1
fi

virt-customize --version >> /dev/null
if [[ ! $? -eq 0 ]]; then
    echo "virt-customize command is not installed. Exit"
    exit 1
fi

# Check for files
if [[ ! -f ${QCOWIMG} ]]; then
    wget "${FEDORAREPO}${QCOWIMG}"
fi
if [[ ! -f ${CHECKSUM} ]]; then
    wget "${FEDORAREPO}${CHECKSUM}"
fi

# Checksum
grep ${QCOWIMG} ${CHECKSUM} | sha256sum -c

if [[ ! $? -eq 0 ]]; then
    echo "The checksum is incorrect. Exit"
    exit 1
fi

# Copy qcow file
cp $(basename $QCOWIMG) $DISKIMAGE

# Copy ssh public key
cp ~/.ssh/id_rsa.pub .

# Customize qcow file
virt-customize -a $DISKIMAGE \
    --hostname ${VMHOSTNAME} \
    --root-password password:$PWROOT \
    --ssh-inject 'root:file:id_rsa.pub' \
    --uninstall cloud-init \
    --selinux-relabel

#    --update \
#    --run-command 'useradd -m -p ${PWROOT} ${USER}' \
#    --ssh-inject '${USER}:file:id_rsa.pub' \
#    --install podman,buildah,skopeo \

rm id_rsa.pub

echo root password is: $PWROOT

sudo mv $DISKIMAGE $KVMDIR

pushd $KVMDIR

sudo virt-install --name $VMNAME --memory 4096 --vcpus 2 \
    --disk $DISKIMAGE --import --os-variant $OSVARIANT --noautoconsole

popd

# sudo virsh start $VMNAME
# sudo virsh shutdown $VMNAM

sleep 5s

sudo  virsh domifaddr $VMNAME

for i in $(sudo virsh list --name)
do
	sudo  virsh domifaddr $i
done

