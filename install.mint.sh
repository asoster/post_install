#!/bin/bash
#
#  install.mint.sh
#
#  Copyright 2024 asoster (https://gitlab.com/asoster/)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.



SYSTEM="apt-transport-https curl nano pwgen conky  lm-sensors ntfs-3g \
        p7zip p7zip-full p7zip-rar file-roller  \
        lvm2 cryptsetup sudo openssh-server sshfs nmap gparted htop numlockx  \
        clinfo xz-utils pixz firewall-config figlet hwinfo \
        powerline powerline-gitstatus python3-powerline  \
        gnome-console gnome-screenshot  gnome-sound-recorder \
        neofetch variety "

VIRTUAL=" qemu-kvm virt-manager seabios ovmf virt-viewer guestfs-tools libguestfs-tools "

MEDIA=" easytag easytag-nautilus wget yt-dlp transmission-gtk \
		exiftags exiftool exiftran libimage-exiftool-perl \
        isomaster mkvtoolnix-gui simplescreenrecorder \
        soundconverter gaupol audacity obs-studio  peek \
        alsamixergui rocminfo pavucontrol handbrake \
        gimp inkscape libwebcam0  epson-inkjet-printer-escpr  \
        fonts-dejavu fonts-dejavu-extra fonts-cascadia-code fonts-powerline  \
        fonts-jetbrains-mono fonts-material-design-icons-iconfont \
        libjs-material-design-lite  libdvdcss2 vlc ffmpeg "

BROWSERS=" chromium firefox brave-browser "

JUEGOS=" 0ad supertuxkart "

DEVEL=" vim geany geany-plugins* git gitk git-gui gcc \
        python3 python3-venv python3-virtualenv  \
        libpq-dev graphviz-dev libtiff-dev python3-neovim  \
        python3-pillow python3-freetype python3-pip \
        python3-pycodestyle python3-flake8 \
        python3-freetype libfreetype6 libfreetype-dev gettext pipenv \
        curl  ack  pylint python3-isort \
        libxml2-dev libxslt-dev libjpeg-tools  libjpeg-turbo-progs \
        codium "

ESPDEV=" mu esptool micropython thonny "

# libvdpau-va-gl  libva-vdpau-driver libva-utils lib

DRIVER=" mesa-vdpau-drivers vdpau-driver-all vdpauinfo  "



curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg  \
    https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" \
    | sudo tee /etc/apt/sources.list.d/brave-browser-release.list


wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg

echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | tee /etc/apt/sources.list.d/vscodium.list



apt update 


apt install -y $SYSTEM 
# apt install -y $GNOME 
apt install -y $MEDIA 
apt install -y $BROWSERS 
apt install -y $VIRTUAL 
apt install -y $DRIVER 
apt install -y $DEVEL 
apt install -y $JUEGOS
