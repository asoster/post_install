#!/bin/bash
#
#  debian.sh
#  
#  Copyright 2019 asoster in (https://gitlab.com/asoster/)
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


#cd /root
#if [ ! -e sources.list ]; then 
#    cp /etc/apt/sources.list ./sources.list
#fi
#sed 's/\ main/\ main contrib non-free/g' sources.list > /etc/apt/sources.list

EXTRA=""

if [ -n $1 ]; then
  EXTRA=" ${1} "
fi

apt-get update -y
apt-get $EXTRA upgrade -y
apt-get $EXTRA dist-upgrade -y


FREE="firmware-linux-free firmware-iwlwifi bluez-firmware"

OTHERNONFREE="firmware-linux firmware-linux-nonfree linux-headers-amd64 \
              broadcom-sta-dkms firmware-b43-installer  firmware-atheros \
              firmware-realtek "

ATIAMD="firmware-linux-nonfree libgl1-mesa-dri xserver-xorg-video-ati"

SYSTEM="nano tmux pwgen conky-all guake lm-sensors fwupd \
        hddtemp ntfs-3g p7zip-full \
        lvm2 cryptsetup  sudo openssh-server sshfs  nmap gparted  htop numlockx \
        chromium firefox-esr  fonts-dejavu fonts-dejavu-extra \
        qemu-kvm virt-manager  torbrowser-launcher gimp inkscape  "

GNOME="evolution dconf-editor gnome-tweak-tool gnome-shell-extension-* \
       gnome-system-*  gnome-power-manager gnome-user-share  \
       gnome-sound-recorder calibre gnome-games gnome-chess "

MEDIA="easytag wget curl  youtube-dl libimage-exiftool-perl transmission-gtk  \
       brasero isomaster mkvtoolnix-gui  soundconverter libdvdread4 \
       vlc telegram-desktop ffmpeg ffmpeg-libs vlc-l10n "



DEVELOP="vim geany geany-plugins* git gitk git-svn gcc pgadmin3 gettext \
         python python-virtualenv python3 python3-virtualenv python-dev \
         python3-dev python-imaging libpq-dev graphviz-dev libtiff-dev \
         python-pil python3-pil python3-freetype libfreetype6 libfreetype6-dev "

JUEGOS="0ad supertuxkart"

#multimedia-menus cdcollect mozilla-adblockplus gnome-firmware 
#python3-freetype pyhton-imaging

#bluez-firmware obexfs obexpushd bluez-obexd obexftp handbrake

for f in $SYSTEM $GNOME $MEDIA $DEVELOP $JUEGOS
do
    apt-get $EXTRA install -y $f
done
