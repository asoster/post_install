#!/bin/bash
#
#  setup.sh
#
#  Copyright 2020 asoster (https://gitlab.com/asoster/)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


ACTION=$(echo "$1" | awk '{print tolower($0)}')
shift
EXTRA=$(echo "$@" | awk '{print tolower($0)}')

# Detect Distro and version

if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
else
    OS=unknown
    VER=unknown
fi

NEW_VER=$((VER + 1))

DISTRO=$(echo "$OS" | awk '{print tolower($0)}')
FILE=""
PACKAGE="all"
COMMAND=""
COMMAND_PRE=""
COMMAND_POST=""

case "$DISTRO" in
    fedora)
      case "$ACTION" in
        install)
          COMMAND_PRE=""
          COMMAND="dnf -y install ${EXTRA}"
          ;;
        update)
          COMMAND="dnf upgrade -y --refresh"
          ;;
        upgrade)
          COMMAND="dnf -y update"
          ;;
        dist-upgrade)
          COMMAND_PRE="dnf upgrade -y --refresh "
          COMMAND="dnf install dnf-plugin-system-upgrade -y && \
                   dnf system-upgrade download --releasever=${NEW_VER} -y "
          COMMAND_POST="dnf system-upgrade reboot"
          ;;
      esac
      ;;
    ubuntu|debian)
      case "$ACTION" in
        install)
          COMMAND_PRE="apt update -y "
          COMMAND="apt -y install ${EXTRA}"
          ;;
        update)
          COMMAND="apt -y update"
          ;;
        upgrade)
          COMMAND_PRE="apt update -y "
          COMMAND="apt -y upgrade"
          ;;
        dist-upgrade)
          COMMAND_PRE="apt update -y && \
                       apt -y upgrade "
          COMMAND="apt -y dist-upgrade"
          ;;
      esac
      ;;
esac

#echo "$COMMAND_PRE"
bash -c "$COMMAND_PRE"

#echo "$COMMAND"
bash -c "$COMMAND"

#echo "$COMMAND_POST"
bash -c "$COMMAND_POST"

